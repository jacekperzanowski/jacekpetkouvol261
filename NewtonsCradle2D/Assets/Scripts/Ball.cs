﻿using UnityEngine;

namespace NewtonsCradle2D
{
    /// <summary>
    /// Single ball in a Newton's Cradle
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(CircleCollider2D))]
    public class Ball : MonoBehaviour
    {
        /// <summary>
        /// Cached rigidbody
        /// </summary>
        private Rigidbody2D _rigidbody2D;

        /// <summary>
        /// Cached collider
        /// </summary>
        private CircleCollider2D _circleCollider2D;

        /// <summary>
        /// Cached camera
        /// </summary>
        private Camera _mainCamera;

        /// <summary>
        /// Indicates if this ball is grabbed by user's mouse
        /// </summary>
        private bool _isGrabbed;



        /// <summary>
        /// Force strength with which this ball is being pulled towards mouse position when grabbed.
        /// </summary>
        [SerializeField]
        [Tooltip("Force strength with which this ball is being pulled towards mouse position when grabbed.")]
        private float grabForce = 10;

        /// <summary>
        /// How much pull force is dampened when grabbed.
        /// </summary>
        [SerializeField]
        [Tooltip("How much pull force is dampened when grabbed.")]
        private float grabForceDampening = 1;



        private void Awake()
        {
            // Cache components for further use
            _rigidbody2D = GetComponent<Rigidbody2D>();
            _circleCollider2D = GetComponent<CircleCollider2D>();
            _mainCamera = Camera.main;
        }

        private void Update()
        {
            // Left mouse button was presses over this ball
            if (Input.GetMouseButtonDown(0) && _circleCollider2D.OverlapPoint(GetMouseWorldPosition()))
            {
                // Grab
                _isGrabbed = true;                
            }

            // Left mouse button was released
            if (Input.GetMouseButtonUp(0))
            {
                // Release
                _isGrabbed = false;
            }

            // Ball is being grabbed
            if (_isGrabbed)
            {
                // Move towards mouse position
                var force = (GetMouseWorldPosition() - (Vector2)transform.position).normalized * grabForce;
                var dampening = _rigidbody2D.velocity * grabForceDampening;
                _rigidbody2D.AddForce(force - dampening);
            }
        }        

        /// <summary>
        /// Returns mouse postion in world space
        /// </summary>
        /// <returns>Mouse position in world space</returns>
        private Vector2 GetMouseWorldPosition()
        {
            return _mainCamera.ScreenToWorldPoint(Input.mousePosition);
        }
    }
}